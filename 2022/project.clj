(defproject vmfhrmfoaj/advent-of-code
  "0.1.0-SNAPSHOT"

  :description "FIXME"
  :url "FIXME"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}

  :main advent-of-code.core

  :source-paths ["src" "spec"]

  :plugins []

  :dependencies [[org.clojure/clojure "1.11.1"]
                 [com.taoensso/timbre "5.2.1"]
                 [com.fzakaria/slf4j-timbre "0.3.21"]
                 [org.clojure/tools.cli "1.0.214"]
                 [com.github.vmfhrmfoaj/clj-toml "1.0.0-SNAPSHOT"]]

  :profiles
  {:uberjar {:aot :all}
   :dev {:source-paths ["gen" "test" "tool"]
         :target-path "dev-target"
         :dependencies [[org.clojure/data.xml "0.0.8"]
                        [org.clojure/test.check "1.1.1"]
                        [org.clojure/tools.deps.alpha "0.14.1222" :exclusions [com.google.guava/guava]]
                        [tortue/spy "2.13.0" :exclusions [org.clojure/clojurescript]]
                        [criterium "0.4.6"]
                        [clj-http "3.12.3"]
                        [environ "1.2.0"]]
         :repl-options {:init-ns advent-of-code.core
                        ;; If you got an error, remove a vector surrounding `do` expression.
                        ;; See, https://github.com/technomancy/leiningen/issues/878
                        :init [(do
                                 (require 'taoensso.timbre)
                                 (taoensso.timbre/merge-config! {:min-level :info ; if you set it to `:debug`, you will see a lot of logs.
                                                                 :timestamp-opts {:timezone (java.util.TimeZone/getDefault)}}))]}
         :auto-refresh ^:replace {:on-reload "dev.tool/on-reload" ; see https://gitlab.com/vmfhrmfoaj/my-lein-utils
                                  :notify-command ["notify-send" "Clojure REPL"]
                                  :verbose false}
         :test-refresh ^:replace {:changes-only true
                                  :timeout-in-sec 5
                                  :notify-command ["notify-send" "Clojure Test"]
                                  ;; `:level`: send a notification whenever a test result is failure
                                  ;; `:edge`: send a notification if a test result is changed (success -> failure or filaure -> success)
                                  :notify-trigger-type :edge}}
   :cicd {:local-repo ".m2/repository"}})
