(ns input
  (:require [clojure.java.io :as io]
            [clj-http.client :as http]
            [clj-http.cookies :as cookie]
            [environ.core :refer [env]])
  (:import org.apache.http.impl.cookie.BasicClientCookie
           java.util.Date))


(defn download
  "Download a puzzle input to the resources directory.

  Example:
   (download 1)"
  ([day]
   (let [date (Date.)
         year (cond-> (+ 1900 (.getYear date))
                (not= 11 (.getMonth date)) (dec))]
     (download year day)))
  ([year day]
   ;; NOTE
   ;;  You need to put `session` cookie into ".lein-env" file in the root of project. (e.g. {:session-key "..."})
   ;;  You can retrieve the cookie using the develoepr tool of browser. See, https://firefox-source-docs.mozilla.org/devtools-user/storage_inspector/cookies/index.html#context-menu
   (if-let [sess (env :session-key)]
     (let [sess-cookie (doto (BasicClientCookie. "session" sess)
                         (.setDomain ".adventofcode.com")
                         (.setPath "/")
                         (.setSecure true))
           cookie-store (doto (cookie/cookie-store)
                          (cookie/add-cookie sess-cookie))
           resp (http/get (str "https://adventofcode.com/" year "/day/" day "/input") {:cookie-store cookie-store})
           out-file (io/as-file (format "resources/day%02d/input.txt" day))]
       (.. out-file (getParentFile) (mkdirs))
       (spit out-file (:body resp)))
     (throw (Exception. "No session key")))))


(comment
  (download (.getDate (Date.))))
