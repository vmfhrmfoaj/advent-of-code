(ns advent-of-code.day06
  (:require [clojure.java.io :as io]
            [criterium.core :refer [quick-bench]])
  (:import (java.io Reader ByteArrayInputStream)))

(set! *warn-on-reflection* true)


(defn day06-common
  [win-sz ^Reader input]
  (let [buf (int-array win-sz)]
    (loop [c (.read input)
           idx 0
           dup-idx 0]
      (when-not (= -1 c)
        (let [dup-idx (reduce (fn [dup-idx idx]
                                (if (= c (aget buf (mod idx win-sz)))
                                  idx
                                  dup-idx))
                              dup-idx
                              (range dup-idx idx))]
          (if (< dup-idx (- idx win-sz))
            [idx (->> (range (- idx win-sz) idx)
                      (map #(aget buf (mod % win-sz)))
                      (map char))]
            (do
              (aset buf (mod idx win-sz) c)
              (recur (.read input) (inc idx) dup-idx))))))))


(defn day06-1
  [^Reader input]
  (day06-common 4 input))


(comment
  (let [str->stream #(-> % (.getBytes) (ByteArrayInputStream.))
        inputs ["mjqjpqmgbljsphdztnvjfqwrcgsmlb"     ; =>  7
                "bvwbjplbgvbhsrlpgdmjqwftvncz"       ; =>  5
                "nppdvjthqldpwncqszvftbrmjlhg"       ; =>  6
                "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"  ; => 10
                "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"]] ; => 11
    (->> inputs
         (map str->stream)
         (map io/reader)
         (map day06-1)))

  (with-open [input (io/reader (io/resource "day06/input.txt"))]
    (println (day06-1 input))))

(comment
  (quick-bench (with-open [input (io/reader (io/resource "day06/input.txt"))]
                 (day06-1 input))))


(defn day06-2
  [^Reader input]
  (day06-common 14 input))


(comment
  (let [str->stream #(-> % (.getBytes) (ByteArrayInputStream.))
        inputs ["mjqjpqmgbljsphdztnvjfqwrcgsmlb"     ; => 19(?)
                "bvwbjplbgvbhsrlpgdmjqwftvncz"       ; => 23
                "nppdvjthqldpwncqszvftbrmjlhg"       ; => 23
                "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"  ; => 29
                "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"]] ; => 26
    (->> inputs
         (map str->stream)
         (map io/reader)
         (map day06-2)))

  (with-open [input (io/reader (io/resource "day06/input.txt"))]
    (println (day06-2 input))))

(comment
  (quick-bench (with-open [input (io/reader (io/resource "day06/input.txt"))]
                 (day06-2 input))))
