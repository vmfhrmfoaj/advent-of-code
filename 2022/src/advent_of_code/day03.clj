(ns advent-of-code.day03
  (:require [clojure.java.io :as io]
            [clojure.set :as set]
            [criterium.core :refer [quick-bench]]))

(set! *warn-on-reflection* true)


(def priority
  (let [items (lazy-cat (range (int \a) (inc (int \z)))
                        (range (int \A) (inc (int \Z))))]
    (as-> items _
      (map char _)
      (zipmap _ (range 1 53))
      (into {} _))))


(defn parse-input-1
  [input]
  (split-at (/ (count input) 2) input))


(defn day03-1
  [input]
  (->> input
       (map parse-input-1)
       (mapcat #(->> %
                     (map set)
                     (apply set/intersection)))
       (map priority)
       (reduce +)))


(comment
  (let [input ["vJrwpWtwJgWrhcsFMMfFFhFp"
               "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"
               "PmmdzqPrVvPwwTWBwg"
               "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"
               "ttgJtRGJQctTZtZT"
               "CrZsJsPPZsGzwwsLwLmpwMDw"]]
    (day03-1 input))

  (with-open [rdr (io/reader (io/resource "day03/input.txt"))]
    (print (day03-1 (line-seq rdr)))))


(defn parse-input-2
  [input]
  (->> input
       (map set)
       (partition 3)))


(defn common-items
  [group]
  (apply set/intersection group))


(defn day03-2
  [input]
  (->> input
       (parse-input-2)
       (mapcat common-items)
       (map priority)
       (reduce +)))


(comment
  (let [input ["vJrwpWtwJgWrhcsFMMfFFhFp"
               "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"
               "PmmdzqPrVvPwwTWBwg"
               ;; ---
               "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"
               "ttgJtRGJQctTZtZT"
               "CrZsJsPPZsGzwwsLwLmpwMDw"]]
    (day03-2 input))

  (with-open [rdr (io/reader (io/resource "day03/input.txt"))]
    (print (day03-2 (line-seq rdr)))))
