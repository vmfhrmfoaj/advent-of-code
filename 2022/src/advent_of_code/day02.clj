(ns advent-of-code.day02
  (:require [clojure.java.io :as io]
            [criterium.core :refer [quick-bench]]))

(set! *warn-on-reflection* true)


(def selections [:rock :paper :sissor :spock :lizard])

(def game-point {:draw 3, :win 6, :lose 0})

(def selection-point {:rock 1, :paper 2, :sissor 3, :spock 4, :lizard 5})


(def translate
  {\A :rock \B :paper \C :sissor \D :spock \E :lizard
   \X :rock \Y :paper \Z :sissor \a :spock \b :lizard})


(defn rotate
  [n coll]
  (let [n (mod n (count coll))]
    (lazy-cat (take-last n coll) (drop-last n coll))))


(defn idx
  [sels sel]
  (let [idx (reduce #(if (= sel %2)
                       (reduced %1)
                       (inc %1))
                    0 sels)]
    (when-not (= sel (nth sels idx))
      (throw (Exception. (str "not found '" sel "' in the range"))))
    idx))


(def game
  (let [rule (->> (iterate #(rotate 1 %) selections)
                  (take (count selections))
                  (map #(take 3 %))
                  (mapcat #(for [oppenent-sel %
                                 my-sel       %]
                             [[oppenent-sel my-sel] (nth (rotate (idx % oppenent-sel) [:draw :win :lose]) (idx % my-sel))]))
                  (into {}))]
    #(rule [%1 %2])))


(defn cal-earned-point
  [opponent-sel my-sel]
  (let [res (game opponent-sel my-sel)
        game-point (game-point res)
        sel-point (selection-point my-sel)]
    (+ game-point sel-point)))


(def parse-input-1
  (juxt (comp translate first)
        (comp translate last)))


(defn day02-1
  [input]
  (->> input
       (map parse-input-1)
       (map #(apply cal-earned-point %))
       (reduce +)))


(comment
  (let [input ["A Y" "B X" "C Z"]]
    (day02-1 input))

  (with-open [rdr (io/reader (io/resource "day02/input.txt"))]
    (println (day02-1 (line-seq rdr)))))

(comment
  (quick-bench (with-open [rdr (io/reader (io/resource "day02/input.txt"))]
                 (day02-1 (line-seq rdr)))))


(def losing-selection
  (into {} (for [sel selections]
             [sel (nth selections (mod (dec (idx selections sel)) (count selections)))])))


(def winning-selection
  (into {} (for [sel selections]
             [sel (nth selections (mod (inc (idx selections sel)) (count selections)))])))


(def parse-input-2
  (juxt (comp translate first) last))


(defn day02-2
  [input]
  (->> input
       (map parse-input-2)
       (map (fn [[opponent-sel instruction]]
              (let [my-sel (condp = instruction
                             \X (losing-selection opponent-sel)
                             \Y opponent-sel
                             \Z (winning-selection opponent-sel))]
                (cal-earned-point opponent-sel my-sel))))
       (reduce +)))


(comment
  (let [input ["A Y" "B X" "C Z"]]
    (day02-2 input))

  (with-open [rdr (io/reader (io/resource "day02/input.txt"))]
    (println (day02-2 (line-seq rdr)))))

(comment
  (quick-bench (with-open [rdr (io/reader (io/resource "day02/input.txt"))]
                 (day02-2 (line-seq rdr)))))
