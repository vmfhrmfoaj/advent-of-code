(ns advent-of-code.day07
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.walk :refer [postwalk]]))

(set! *warn-on-reflection* true)


(defmulti update-state--cmd
  (fn [_state cmd _arg]
    cmd))


(defmethod update-state--cmd "cd"
  [state cmd arg]
  (-> state
      (assoc :last-cmd cmd)
      (update :cwd (fn [path]
                     (let [target-dir arg]
                       (cond
                         (= ".." target-dir)
                         (pop path)

                         (str/starts-with? target-dir "/")
                         (->> (str/split target-dir #"/")
                              (remove str/blank?)
                              (into []))

                         :else (conj path target-dir)))))))


(defmethod update-state--cmd "ls"
  [state cmd _arg]
  (assoc state :last-cmd cmd))


(defmulti update-state--res
  (fn [{:keys [last-cmd]} _line]
    last-cmd))


(defmethod update-state--res "ls"
  [{:keys [cwd] :as state} line]
  (update state :dir
          (fn [dir]
            (let [dir-matcher (re-matcher #"^dir\s+([^ ]+)" line)
                  file-matcher (re-matcher #"^(\d+)\s+([^ ]+)" line)]
              (cond
                (re-find dir-matcher)
                (let [[_ name] (re-groups dir-matcher)]
                  (assoc-in dir (conj cwd name) {:type :dir, :size 0}))

                (re-find file-matcher)
                (let [[_ sz name] (re-groups file-matcher)]
                  (assoc-in dir (conj cwd name) {:type :file, :size (Long/parseLong sz)})))))))


(defn update-dir-sz
  [dir]
  (postwalk (fn [x]
              (cond-> x
                (and (map? x) (= :dir (x :type)))
                (assoc :size (->> (vals x)
                                  (filter map?)
                                  (map :size)
                                  (reduce +)))))
            dir))


(defn parse-input
  [input]
  (->> input
       (reduce (fn [state line]
                 (if-let [[_ cmd arg] (re-matches #"^\$\s+(\w+)(?:\s+([^ ]+))*$" line)]
                   (update-state--cmd state cmd arg)
                   (update-state--res state line)))
               {:cwd ["."]
                :dir {:type :dir, :size 0}
                :last-cmd nil
                :req-res? false})
       (update-dir-sz)))


(defn all-sub-dirs
  [dir]
  (->> dir
       (map val)
       (filter map?)
       (filter #(= :dir (:type %)))
       (mapcat #(cons % (all-sub-dirs %)))))


(defn day07-1
  [input]
  (let [base-sz 100000
        {:keys [dir]} (parse-input input)]
    (->> (all-sub-dirs dir)
         (map :size)
         (filter #(> base-sz %))
         (reduce +))))


(comment
  (let [input ["$ cd /"
               "$ ls"
               "dir a"
               "14848514 b.txt"
               "8504156 c.dat"
               "dir d"
               "$ cd a"
               "$ ls"
               "dir e"
               "29116 f"
               "2557 g"
               "62596 h.lst"
               "$ cd e"
               "$ ls"
               "584 i"
               "$ cd .."
               "$ cd .."
               "$ cd d"
               "$ ls"
               "4060174 j"
               "8033020 d.log"
               "5626152 d.ext"
               "7214296 k"]]
    (day07-1 input))

  (with-open [rdr (io/reader (io/resource "day07/input.txt"))]
    (println (day07-1 (line-seq rdr)))))


(defn day07-2
  [input]
  (let [total-avail-sz  70000000
        desired-free-sz 30000000
        {:keys [dir]} (parse-input input)
        free-sz (- total-avail-sz (dir :size))
        target-sz (- desired-free-sz free-sz)]
    (->> (all-sub-dirs dir)
         (reduce (fn [[_ selected-dir-diff :as sel] {:keys [size] :as dir}]
                   (let [diff (- size target-sz)]
                     (if (and (<= 0 diff) (> selected-dir-diff diff))
                       [dir diff]
                       sel)))
                 [dir (- (dir :size) target-sz)])
         (first)
         (:size))))


(comment
  (let [input ["$ cd /"
               "$ ls"
               "dir a"
               "14848514 b.txt"
               "8504156 c.dat"
               "dir d"
               "$ cd a"
               "$ ls"
               "dir e"
               "29116 f"
               "2557 g"
               "62596 h.lst"
               "$ cd e"
               "$ ls"
               "584 i"
               "$ cd .."
               "$ cd .."
               "$ cd d"
               "$ ls"
               "4060174 j"
               "8033020 d.log"
               "5626152 d.ext"
               "7214296 k"]]
    (day07-2 input))

  (with-open [rdr (io/reader (io/resource "day07/input.txt"))]
    (println (day07-2 (line-seq rdr)))))
