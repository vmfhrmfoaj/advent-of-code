(ns advent-of-code.day05
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.math :as math]
            [criterium.core :refer [quick-bench]]))

(set! *warn-on-reflection* true)


(defn parse-stack
  [input]
  (let [stack (butlast input)
        id-line (last input)
        ids (map second (re-seq #"(?:\s(\w+)\s)" id-line))
        col-len (int (math/ceil (/ (count id-line) (count ids))))]
    [ids (zipmap ids (reduce (fn [stack line]
                               (let [line-sz (count line)
                                     cols (loop [start 0
                                                 cols []]
                                            (if (<= (count ids) (count cols))
                                              cols
                                              (let [end (min line-sz (+ start col-len))]
                                                (recur end (conj cols (subs line start end))))))]
                                 (->> cols
                                      (map (fn [col]
                                             (when-let [start (str/index-of col "[")]
                                               (when-let [end (str/index-of col "]" start)]
                                                 (subs col (inc start) end)))))
                                      (map #(cond-> %1
                                              %2 (conj %2))
                                           stack))))
                             (repeat (count ids) '())
                             stack))]))


(defn parse-procedure
  [input]
  (let [[_ quantity from to] (re-find #"move (\d+) from (\d+) to (\d+)" input)]
    {:from from
     :to to
     :quantity (Integer/parseInt quantity)}))


(defn process
  [stack
   {:keys [from to quantity]                         :as _procedure} &
   {:keys [keep-order?]      :or {keep-order? false} :as _opts}]
  (let [from-crates (stack from)
        break-pos (- (count from-crates) quantity)
        [remaining-crates transfer-crates] (split-at break-pos from-crates)]
    (-> stack
        (assoc from remaining-crates)
        (update to concat (cond-> transfer-crates
                            (not keep-order?)
                            (reverse))))))


(defn parse-input
  [input]
  (let [[stack procedures] (split-with #(not (str/blank? %)) input)
        [ids stack] (parse-stack stack)]
    [ids stack (->>  procedures
                     (drop 1)
                     (map parse-procedure))]))


(defn day05-1
  [input]
  (let [[ids stack procedures] (parse-input input)
        stack (reduce #(process %1 %2) stack procedures)]
    (apply str (map #(last (stack %)) ids))))


(comment
  (let [input ["    [D]    "
               "[N] [C]    "
               "[Z] [M] [P]"
               " 1   2   3 "
               ""
               "move 1 from 2 to 1"
               "move 3 from 1 to 3"
               "move 2 from 2 to 1"
               "move 1 from 1 to 2"]]
    (day05-1 input))

  (with-open [rdr (io/reader (io/resource "day05/input.txt"))]
    (println (day05-1 (line-seq rdr)))))

(comment
  (quick-bench (with-open [rdr (io/reader (io/resource "day05/input.txt"))]
                 (day05-1 (line-seq rdr)))))


(defn day05-2
  [input]
  (let [[ids stack procedures] (parse-input input)
        stack (reduce #(process %1 %2 :keep-order? true) stack procedures)]
    (apply str (map #(last (stack %)) ids))))


(comment
  (let [input ["    [D]    "
               "[N] [C]    "
               "[Z] [M] [P]"
               " 1   2   3 "
               ""
               "move 1 from 2 to 1"
               "move 3 from 1 to 3"
               "move 2 from 2 to 1"
               "move 1 from 1 to 2"]]
    (day05-2 input))

  (with-open [rdr (io/reader (io/resource "day05/input.txt"))]
    (println (day05-2 (line-seq rdr)))))

(comment
  (quick-bench (with-open [rdr (io/reader (io/resource "day05/input.txt"))]
                 (day05-2 (line-seq rdr)))))
