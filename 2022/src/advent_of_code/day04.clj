(ns advent-of-code.day04
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [criterium.core :refer [quick-bench]]))

(set! *warn-on-reflection* true)


(defn to-sec-range
  [x-to-y]
  (let [[x y] (str/split x-to-y #"-")]
    (into #{} (range (Integer/parseInt x) (inc (Integer/parseInt y))))))


(defn parse-input
  [input]
  (map to-sec-range (str/split input #",")))


(defn subset?
  [a b]
  (if (< (count a) (count b))
    (subset? b a)
    (every? #(a %) b)))


(defn day04-1
  [input]
  (->> input
       (map parse-input)
       (filter #(apply subset? %))
       (count)))


(comment
  (let [input ["2-4,6-8"
               "2-3,4-5"
               "5-7,7-9"
               "2-8,3-7"
               "6-6,4-6"
               "2-6,4-8"]]
    (day04-1 input))

  (with-open [rdr (io/reader (io/resource "day04/input.txt"))]
    (println (day04-1 (line-seq rdr)))))

(comment
  (quick-bench (with-open [rdr (io/reader (io/resource "day04/input.txt"))]
                 (day04-1 (line-seq rdr)))))


(defn overlap?
  [a b]
  (some #(b %) a))


(defn day04-2
  [input]
  (->> input
       (map parse-input)
       (filter #(apply overlap? %))
       (count)))


(comment
  (let [input ["2-4,6-8"
               "2-3,4-5"
               "5-7,7-9"
               "2-8,3-7"
               "6-6,4-6"
               "2-6,4-8"]]
    (day04-2 input))

  (with-open [rdr (io/reader (io/resource "day04/input.txt"))]
    (println (day04-2 (line-seq rdr)))))

(comment
  (quick-bench (with-open [rdr (io/reader (io/resource "day04/input.txt"))]
                 (day04-2 (line-seq rdr)))))
