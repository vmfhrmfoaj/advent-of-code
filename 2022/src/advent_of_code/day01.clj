(ns advent-of-code.day01
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [criterium.core :refer [quick-bench]]))

(set! *warn-on-reflection* true)


(defn total-calorie
  [calories]
  (reduce + (map #(Integer/parseInt %) calories)))


(defn elf-total-calories
  [input]
  (->> input
       (partition-by str/blank?)
       (remove #(str/blank? (first %)))
       (map total-calorie)))


(defn day01-1
  [input]
  (apply max (elf-total-calories input)))


(comment
  (let [calories ["1000" "2000" "3000" ""
                  "4000" ""
                  "5000" "6000" ""
                  "7000" "8000" "9000" ""
                  "10000"]]
    (day01-1 calories))

  (with-open [rdr (io/reader (io/resource "day01/input.txt"))]
    (println (day01-1 (line-seq rdr)))))

(comment
  (quick-bench (with-open [rdr (io/reader (io/resource "day01/input.txt"))]
                 (day01-1 (line-seq rdr)))))


(defn day01-2
  ([input]
   (day01-2 input 3))
  ([input n]
   (reduce + (reduce #(take n (sort > (conj %1 %2))) [] (elf-total-calories input)))))


(comment
  (let [calories ["1000" "2000" "3000" ""
                  "4000" ""
                  "5000" "6000" ""
                  "7000" "8000" "9000" ""
                  "10000"]]
    (day01-2 calories))

  (with-open [rdr (io/reader (io/resource "day01/input.txt"))]
    (println (day01-2 (line-seq rdr)))))

(comment
  (quick-bench (with-open [rdr (io/reader (io/resource "day01/input.txt"))]
                 (day01-2 (line-seq rdr)))))
