(ns advent-of-code.core-gen
  (:require [clojure.spec.gen.alpha :as gen]
            [advent-of-code.core-spec :as target-spec]
            [advent-of-code.test-utils :as utils]))


(utils/attach-gen! ::target-spec/simple (constantly (gen/elements (mapv str "simple"))))


(comment
  (require '[clojure.test.check.generators])
  (require '[clojure.test.check.rose-tree :as rose])
  (import 'clojure.test.check.generators.Generator)
  (import 'java.util.UUID)

  ;; NOTE
  ;;  How to create custom generator:
  (gen/sample
   (Generator.
    (fn [_rng _size]
      (rose/make-rose (UUID/randomUUID) [])))))
