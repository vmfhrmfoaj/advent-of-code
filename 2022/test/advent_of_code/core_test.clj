(ns advent-of-code.core-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as spec]
            [clojure.spec.gen.alpha :as gen]
            [advent-of-code.core :as target]
            [advent-of-code.core-spec :as target-spec]
            [advent-of-code.core-gen]
            [advent-of-code.test-utils :as utils]
            [spy.core :as spy]))


(use-fixtures :once utils/without-trivial-log)


(deftest example
  (testing "how to use `spy`"
    (is (let [x (spy/stub nil)]
          (x 1 2 3 4)
          (spy/called-with? x 1 2 3 4))))

  (testing "how to use `clojure.spec.gen`"
    (is (get (->> "simple"
                  (map str)
                  (into #{}))
             (gen/generate (spec/gen ::target-spec/simple))))))
