(ns advent-of-code.test-utils
  (:require [clojure.spec.alpha :as spec]
            [taoensso.timbre :as log]))


(defn attach-gen!
  "Attatch a generator to a spec.

  Example:
   (attatch-gen ::token #(gen/fmap str (gen/uuid)))"
  [spec gen-fn]
  (and (get (swap! @#'spec/registry-ref (fn [registry]
                                          (cond-> registry
                                            (get registry spec)
                                            (update spec #(spec/with-gen % gen-fn)))))
            spec)
       true))


(defn without-trivial-log
  "Set log level to error to ignore debugging logs.

  Example:
   (clojure.test/use-fixtures :once without-trivial-log)"
  [func]
  (log/merge-config! {:min-level :error})
  (func))
