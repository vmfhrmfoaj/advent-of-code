(ns advent-of-code.day02-test
  (:require [clojure.test :refer :all]
            [advent-of-code.day02 :as target]))


(deftest game
  (testing "draw"
    (is (= :draw (target/game :rock   :rock)))
    (is (= :draw (target/game :paper  :paper)))
    (is (= :draw (target/game :sissor :sissor)))
    (is (= :draw (target/game :spock  :spock)))
    (is (= :draw (target/game :lizard :lizard))))

  (testing "win"
    (is (= :win (target/game :lizard :rock)))
    (is (= :win (target/game :sissor :rock)))
    (is (= :win (target/game :rock   :paper)))
    (is (= :win (target/game :spock  :paper)))
    (is (= :win (target/game :paper  :sissor)))
    (is (= :win (target/game :lizard :sissor)))
    (is (= :win (target/game :sissor :spock)))
    (is (= :win (target/game :rock   :spock)))
    (is (= :win (target/game :spock  :lizard)))
    (is (= :win (target/game :paper  :lizard))))

  (testing "lose"
    (is (= :lose (target/game :paper  :rock)))
    (is (= :lose (target/game :spock  :rock)))
    (is (= :lose (target/game :sissor :paper)))
    (is (= :lose (target/game :lizard :paper)))
    (is (= :lose (target/game :spock  :sissor)))
    (is (= :lose (target/game :rock   :sissor)))
    (is (= :lose (target/game :lizard :spock)))
    (is (= :lose (target/game :paper  :spock)))
    (is (= :lose (target/game :rock   :lizard)))
    (is (= :lose (target/game :sissor :lizard)))))
