package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
)

type MeasureWindow struct {
	measures   []int
	capacity   int
	windowSize int
	full       bool
	head       int
	tail       int
}

func NewMeasureWindow(wndSz uint, preservation uint) *MeasureWindow {
	cap := wndSz + preservation
	mw := MeasureWindow{
		measures:   make([]int, cap),
		capacity:   int(cap),
		windowSize: int(wndSz),
		full:       false,
		head:       0,
		tail:       0}
	return &mw
}

func (mw *MeasureWindow) Push(data int) {
	mw.measures[mw.tail] = data
	mw.tail = (mw.tail + 1) % mw.capacity

	if mw.full {
		mw.head = (mw.head + 1) % mw.capacity // drop latest data
	} else if mw.head == mw.tail {
		mw.full = true
	}
}

// func (mw *MeasureWindow) GetData() (int, error) {
// 	idx := mw.head
// 	data := 0
// 	for i := 0; i < mw.windowSize; i++ {
// 		if idx == mw.tail {
// 			return 0, errors.New("not enough data")
// 		}
//
// 		data += mw.measures[idx]
// 		idx = (idx + 1) % mw.capacity
// 	}
// 	return data, nil
// }

// func (mw *MeasureWindow) GetAllData(n int) ([]int, error) {
// 	p := mw.capacity - mw.windowSize
// 	if p == 0 {
// 		data, err := mw.GetData()
// 		if err != nil {
// 			return nil, err
// 		}
// 		return []int{data}, nil
// 	}
// 	if mw.head == mw.tail {
// 		return nil, errors.New("not enough data")
// 	}
//
// 	var data []int
// 	idx := mw.head
// 	comm := 0
// 	for i := 0; i < p; i++ {
// 		d := mw.measures[idx]
// 		idx = (idx + 1) % mw.capacity
// 		if idx == mw.tail {
// 			if len(data) > 0 {
// 				return data, nil
// 			}
// 			return nil, errors.New("not enough data")
// 		}
// 		for k := 1; k < mw.windowSize; k++ {
// 			if idx == mw.tail {
// 				if len(data) > 0 {
// 					return data, nil
// 				}
// 				return nil, errors.New("not enough data")
// 			}
// 			comm += mw.measures[idx]
// 			idx = (idx + 1) % mw.capacity
// 		}
//
// 		data = append(data, d+comm)
// 	}

// 	return data, nil
// }

func (mw *MeasureWindow) IsIncreased() (bool, error) {
	if !mw.full {
		return false, errors.New("not enough data")
	}

	prev := mw.measures[mw.head]
	cur := mw.measures[(mw.tail-1)%mw.capacity]
	if mw.windowSize <= 2 {
		for i := 1; i <= mw.windowSize; i++ {
			n := mw.measures[(mw.head+i)%mw.capacity]
			prev += n
			cur += n
		}
	}
	return prev < cur, nil
}

const defaultMeasureWndSz uint = 3

func GetArgs() (*os.File, uint, error) {
	numOfArgs := len(os.Args)
	if numOfArgs < 2 {
		return nil, 0, fmt.Errorf("Usage: %s <INPUT-FILE> [<WINDOW-SIZE(default: 3)>]", os.Args[0])
	}
	x := os.Args[1]
	if _, err := os.Stat(x); err != nil {
		return nil, 0, fmt.Errorf("Unable to read [%s] file: file not exists", x)
	}
	file, err := os.Open(x)
	if err != nil {
		return nil, 0, fmt.Errorf("Unable to read [%s] file: %s", x, err)
	}

	wndSz := defaultMeasureWndSz
	if numOfArgs >= 3 {
		second_arg := os.Args[2]
		x, err := strconv.Atoi(second_arg)
		if err != nil || x <= 0 {
			return nil, 0, fmt.Errorf("The second arguement(WINDOW-SIZE) should be positive number: [%s]", second_arg)
		}
		wndSz = uint(x)
	}

	return file, wndSz, nil
}

func main() {
	f, wndSz, err := GetArgs()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
	defer f.Close()

	numInc := 0
	mw := NewMeasureWindow(wndSz, 1)
	sc := bufio.NewScanner(f)
	for i := uint(0); i < wndSz && sc.Scan(); i++ {
		x := sc.Text()
		n, err := strconv.Atoi(x)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Invalid input: [%s]\n", x)
			os.Exit(1)
		}
		mw.Push(n)
	}
	for sc.Scan() {
		x := sc.Text()
		n, err := strconv.Atoi(x)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Invalid input: [%s]\n", x)
			os.Exit(1)
		}
		mw.Push(n)
		if incp, e := mw.IsIncreased(); e != nil {
			fmt.Fprintf(os.Stderr, "Internal error: %s\n", e)
			os.Exit(1)
		} else if incp {
			numInc++
		}
	}

	if e := sc.Err(); e != nil {
		fmt.Fprintf(os.Stderr, "Unable to read input file: %s\n", e)
		os.Exit(1)
	}

	fmt.Printf("Answer is [%d]\n", numInc)
}
