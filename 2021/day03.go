package main

import (
	"bufio"
	"container/list"
	"fmt"
	"os"
	"strconv"
)

type BitCounter struct{ Low, High uint }

type Report struct {
	measures *list.List
	bitCount map[int]BitCounter
}

func NewReport(sc *bufio.Scanner) (*Report, error) {
	m := list.New()
	for sc.Scan() {
		m.PushBack(sc.Text())
	}
	if e := sc.Err(); e != nil {
		return nil, e
	}

	r := Report{measures: m, bitCount: make(map[int]BitCounter)}
	r.UpdateFrequency()

	return &r, nil
}

func (r *Report) Clone() *Report {
	m := list.New()
	for i := r.measures.Front(); i != nil; i = i.Next() {
		m.PushBack(i.Value)
	}

	bc := make(map[int]BitCounter)
	for k, v := range r.bitCount {
		bc[k] = BitCounter{v.Low, v.High}
	}

	return &Report{measures: m, bitCount: bc}
}

func (r *Report) UpdateFrequency() {
	lc := make(map[int]uint)
	hc := make(map[int]uint)
	for i := r.measures.Front(); i != nil; i = i.Next() {
		x := i.Value.(string)
		for i, bit := range x {
			if bit == '0' {
				lc[i]++
			} else {
				hc[i]++
			}
		}
	}

	for i := 0; i < 64; i++ {
		l, lo := lc[i]
		h, ho := hc[i]
		if !lo && !ho {
			break
		}
		r.bitCount[i] = BitCounter{Low: l, High: h}
	}
}

func (r *Report) GetMostCommonBits() *[]byte {
	bs := make([]byte, len(r.bitCount))
	for i, bitFreq := range r.bitCount {
		b := byte(0)
		if bitFreq.Low <= bitFreq.High { // assumed '1' is preferred
			b = 1
		}
		bs[i] = b
	}
	return &bs
}

func (r *Report) GetPowerConsumption() uint64 {
	bs := r.GetMostCommonBits()

	gamma := uint64(0)
	for _, bit := range *bs {
		gamma = (gamma << 1) | uint64(bit)
	}

	sz := len(*bs)
	mask := uint64(0)
	for i := 0; i < sz; i++ {
		mask = (mask << 1) | 1
	}
	epsilon := gamma ^ mask
	return gamma * epsilon
}

func (r *Report) FindLifeRating() (int64, error) {
	o2, co2 := r.Clone(), r.Clone()

	sz := len(r.bitCount)
	for i := 0; i < sz; i++ {
		o2Bit := '0'
		if o2.bitCount[i].Low <= o2.bitCount[i].High {
			o2Bit = '1'
		}
		j := o2.measures.Front()
		if j.Next() != nil {
			for j != nil {
				j_ := j.Next()

				if o2Bit != []rune(j.Value.(string))[i] {
					o2.measures.Remove(j)
					o2.UpdateFrequency()
				}

				j = j_
			}
		}

		co2Bit := '1'
		if co2.bitCount[i].Low <= co2.bitCount[i].High {
			co2Bit = '0'
		}
		j = co2.measures.Front()
		if j.Next() != nil {
			for j != nil {
				j_ := j.Next()

				if co2Bit != []rune(j.Value.(string))[i] {
					co2.measures.Remove(j)
					co2.UpdateFrequency()
				}

				j = j_
			}
		}
	}

	o2r, e := strconv.ParseInt(o2.measures.Front().Value.(string), 2, 64)
	if e != nil {
		return 0, nil
	}
	co2r, e := strconv.ParseInt(co2.measures.Front().Value.(string), 2, 64)
	if e != nil {
		return 0, nil
	}

	return o2r * co2r, nil
}

func GetArgs() (*os.File, error) {
	if len(os.Args) < 2 {
		return nil, fmt.Errorf("Usage: %s <INPUT-FILE>", os.Args[0])
	}
	x := os.Args[1]
	if _, e := os.Stat(x); e != nil {
		return nil, fmt.Errorf("Unable to read [%s] file: file not exists", x)
	}
	f, e := os.Open(x)
	if e != nil {
		return nil, fmt.Errorf("Unable to read [%s] file: %s", x, e)
	}

	return f, nil
}

func main() {
	f, e := GetArgs()
	if e != nil {
		fmt.Fprintf(os.Stderr, "%s\n", e)
		os.Exit(1)
	}
	defer f.Close()

	r, e := NewReport(bufio.NewScanner(f))
	if e != nil {
		fmt.Fprintf(os.Stderr, "Unable to read input file: %s\n", e)
		os.Exit(1)
	}

	pc := r.GetPowerConsumption()
	lr, e := r.FindLifeRating()
	if e != nil {
		fmt.Fprintf(os.Stderr, "%s\n", e)
		os.Exit(1)
	}

	fmt.Printf("Answer is [%d] and [%d]\n", pc, lr)
}
