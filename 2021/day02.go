package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Positional interface {
	GetXY() (int, int)
	Forward(n int)
	Up(n int)
	Down(n int)
}

type Position struct {
	x, y int
}

func NewPosition() *Position {
	return &Position{x: 0, y: 0}
}

func (pos *Position) GetXY() (int, int) {
	return pos.x, pos.y
}

func (pos *Position) Forward(n int) {
	pos.x += n
}

func (pos *Position) Up(n int) {
	pos.y -= n
}

func (pos *Position) Down(n int) {
	pos.y += n
}

type PositionWithAim struct {
	Position
	aim int
}

func NewPositionAim() *PositionWithAim {
	return &PositionWithAim{
		Position: Position{x: 0, y: 0},
		aim:      0}
}

func (pos *PositionWithAim) Forward(n int) {
	pos.x += n
	pos.y += pos.aim * n
}

func (pos *PositionWithAim) Up(n int) {
	pos.aim -= n
}

func (pos *PositionWithAim) Down(n int) {
	pos.aim += n
}

func GetArgs() (*os.File, bool, error) {
	numArgs := len(os.Args)
	if numArgs < 2 {
		return nil, false, fmt.Errorf("Usage: %s <INPUT-FILE> [<AIM-MODE (default: 0)>]\n", os.Args[0])
	}

	input := os.Args[1]
	if _, e := os.Stat(input); e != nil {
		return nil, false, fmt.Errorf("Unable to read [%s] file: file not exists\n", input)
	}

	f, e := os.Open(input)
	if e != nil {
		return nil, false, fmt.Errorf("Unable to read [%s] file: %s\n", input, e)
	}

	aimp := false
	if numArgs >= 3 {
		if os.Args[2] == "1" {
			aimp = true
		}
	}

	return f, aimp, nil
}

func main() {
	f, aimp, e := GetArgs()
	if e != nil {
		fmt.Fprintf(os.Stderr, "%s\n", e)
		os.Exit(1)
	}
	defer f.Close()

	var pos Positional
	if aimp {
		pos = NewPositionAim()
	} else {
		pos = NewPosition()
	}

	sc := bufio.NewScanner(f)
	for sc.Scan() {
		x := sc.Text()
		fs := strings.Fields(x)
		if len(fs) < 2 {
			fmt.Fprintf(os.Stderr, "Invalid input: [%s]", x)
			os.Exit(1)
		}
		c := fs[0]
		p, e := strconv.Atoi(fs[1])
		if e != nil {
			fmt.Fprintf(os.Stderr, "Invalid input: [%s]", x)
			os.Exit(1)
		}

		switch c {
		case "forward":
			pos.Forward(p)
		case "up":
			pos.Up(p)
		case "down":
			pos.Down(p)
		}
	}

	x, y := pos.GetXY()
	fmt.Printf("Answer is [%d]\n", x*y)
}
